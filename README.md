## Information about Explorers (Crews and Planets) application:
In order to start the database, run:

```docker-compose up```

After the db starts, run the next sql command in Sql ServerManagement Studio in order to create the table "Explorers".
(If I had more time I would to this automatically)
```
CREATE DATABASE Explorers
GO
```
#
####If you want to have no authorization rights required in the applications there is a possibility to deactivate in the application-dev.properties: 
```
application.security.authentication.authorizationActive= false
```
#
####Crews application link to swagger-ui:
http://localhost:8090/crews/swagger-ui.html

####Planets applicationto swagger-ui:
http://localhost:8080/planets/swagger-ui.html
