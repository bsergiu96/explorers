package com.example.crews.core.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "crews")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Crew {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "captain")
	private String captain;
	@OneToMany(mappedBy = "crew", fetch = FetchType.EAGER)
	private List<Robot> robots;

}
