package com.example.crews.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "robots")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Robot {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "name", unique = true)
	private String name;
	@ManyToOne
	@JsonIgnore
	private Crew crew;

	@Override
	public String toString() {
		return "Robot{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
