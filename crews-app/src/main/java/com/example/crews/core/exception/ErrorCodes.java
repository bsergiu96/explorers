package com.example.crews.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Enum to define error codes and additional info.
 */
public enum  ErrorCodes {
	// success (2xx format)
	NO_CONTENT(HttpStatus.NO_CONTENT.value(), 20401),

	// Client errors(4xx format)
	BAD_REQUEST(HttpStatus.BAD_REQUEST.value(), 40001),

	// 409 Unauthorized
	UNAUTHORIZED(HttpStatus.UNAUTHORIZED.value(), 40101),
	AUTHENTICATION_ERROR(HttpStatus.UNAUTHORIZED.value(), 40102),

	// 409 Not Found
	NOT_FOUND(HttpStatus.NOT_FOUND.value(), 40401),
	NO_CONTENT_FOUND(HttpStatus.NOT_FOUND.value(), 40402),

	// 409 Conflict
	EMAIL_EXISTS_ERROR(HttpStatus.UNAUTHORIZED.value(), 40901),


	// Server errors (5xx format)
	INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), 50001),
	NOT_IMPLEMENTED(HttpStatus.NOT_IMPLEMENTED.value(), 50101),
	BAD_GATEWAY(HttpStatus.BAD_GATEWAY.value(), 50201);

	private int httpStatusCode;
	private int code;

	ErrorCodes(int httpStatusCode, int code) {
		this.httpStatusCode = httpStatusCode;
		this.code = code;
	}


	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
