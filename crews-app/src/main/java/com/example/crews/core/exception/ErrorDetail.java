package com.example.crews.core.exception;

import java.io.Serializable;

import lombok.Data;

/**
 * Wraps all information needed to handle the exception
 */
@Data
public class ErrorDetail implements Serializable {
	private String message;
	private int httpStatusCode;
	private ErrorCodes errorCodes;

	public ErrorDetail(String message, ErrorCodes errorCodes) {
		this.message = message;
		this.errorCodes = errorCodes;
		this.httpStatusCode = errorCodes.getHttpStatusCode();
	}
}
