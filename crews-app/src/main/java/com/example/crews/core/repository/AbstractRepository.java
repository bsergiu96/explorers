package com.example.crews.core.repository;


import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AbstractRepository<Entity, ID extends Serializable> extends JpaRepository<Entity, ID>, JpaSpecificationExecutor<Entity> {
}
