package com.example.crews.core.repository;

import com.example.crews.core.domain.Crew;

public interface CrewRepository extends AbstractRepository<Crew, Long> {
}
