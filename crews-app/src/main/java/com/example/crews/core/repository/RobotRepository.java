package com.example.crews.core.repository;

import com.example.crews.core.domain.Robot;

public interface RobotRepository extends AbstractRepository<Robot, Long> {
}
