package com.example.crews.core.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.example.crews.web.rest.common.PathConstants;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class TokenProvider {

	@Value("${application.security.authentication.jwt.secret}")
	private String secretKey;

	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
			return true;
		} catch (Exception e) {
			log.info("Invalid JWT token [{}].", authToken, e);
		}
		return false;
	}

	public String resolveToken(HttpServletRequest request) {
		String bearerToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(PathConstants.AUTHORIZATION_HEADER_VALUE_PREFIX)) {
			return bearerToken.substring(PathConstants.AUTHORIZATION_HEADER_VALUE_PREFIX.length());
		}
		return null;
	}
}
