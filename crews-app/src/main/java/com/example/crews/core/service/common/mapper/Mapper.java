package com.example.crews.core.service.common.mapper;

public interface Mapper<ENTITY, DTO> {
	ENTITY convertDTOToEntity(DTO dto);

	DTO convertEntityToDTO(ENTITY entity);
}
