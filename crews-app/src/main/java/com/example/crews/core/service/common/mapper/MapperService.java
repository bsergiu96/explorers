package com.example.crews.core.service.common.mapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.example.crews.core.exception.ApplicationException;
import com.example.crews.core.exception.ErrorCodes;
import com.example.crews.core.exception.ErrorDetail;
import com.google.common.reflect.ClassPath;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MapperService implements ApplicationListener<ContextRefreshedEvent> {
	private Map<String, Mapper> availableMappers = new HashMap<>();

	public Mapper getMapperService(Class classArg) {return availableMappers.get(classArg.getName());}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		final Collection<Mapper> mappers = event.getApplicationContext().getBeansOfType(Mapper.class).values();
		final List<Class> domainClasses = getDomainClasses() ;

		if (domainClasses != null) {
			for (final Class domainClass : domainClasses) {
				mappers.stream().filter(mapper -> match(mapper, domainClass)).forEach(mapper -> {
					availableMappers.put(domainClass.getName(), mapper);
					log.info("Adding mapper for class [{}]", domainClass);
				});
			}
		} else {
			log.warn("No domain classes were found. There is something wrong.");
		}
	}

	private List<Class> getDomainClasses(){
		final ClassLoader loader = getClass().getClassLoader();
		try {
			final String domainObjectPackage = "com.example.crews.core.domain";
			ClassPath.from(loader).getAllClasses().stream().forEach(classInfo -> {
				int i = 0;
				if (classInfo.getName().contains("com.example")) {
					i += 1;
				}
			});
			return ClassPath.from(loader).getAllClasses().stream().filter(classInfo -> {
				return classInfo.getName().contains(domainObjectPackage);
			}).map(ClassPath.ClassInfo::getName).map(className -> className.replaceFirst("BOOT-INF.classes.","")).
					map(className -> {
						try {
							return loader.loadClass(className);
						} catch (ClassNotFoundException e) {
							throw new ApplicationException(
									new ErrorDetail("Error occurred while search for class " + e.getMessage(), ErrorCodes.INTERNAL_SERVER_ERROR));
						}
					}).collect(Collectors.toList());
		} catch (Exception e) {
			throw new ApplicationException(
					new ErrorDetail("Error occurred while getting domain class " + e.getMessage(), ErrorCodes.INTERNAL_SERVER_ERROR));
		}
	}

	private boolean match(Mapper mapper, Class domainClass) {
		return mapper.getClass().getSimpleName().replace("Mapper", "").replace("Impl", "").equals(domainClass
				.getSimpleName());
	}
}
