package com.example.crews.core.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.crews.core.domain.Crew;
import com.example.crews.core.domain.Robot;
import com.example.crews.core.exception.ApplicationException;
import com.example.crews.core.exception.ErrorCodes;
import com.example.crews.core.exception.ErrorDetail;
import com.example.crews.core.repository.AbstractRepository;
import com.example.crews.core.repository.CrewRepository;
import com.example.crews.core.repository.RobotRepository;
import com.example.crews.core.service.common.AbstractService;
import com.example.crews.web.rest.controlers.dto.CrewDTO;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class CrewServiceImpl extends AbstractService<Long, Crew, CrewDTO> {
	private final CrewRepository crewRepository;
	private final RobotRepository robotRepository;

	public CrewServiceImpl(CrewRepository crewRepository, RobotRepository robotRepository) {
		super(Crew.class);
		this.crewRepository = crewRepository;
		this.robotRepository = robotRepository;
	}

	@Override
	protected AbstractRepository<Crew, Long> getRepository() {
		return crewRepository;
	}

	public List<Crew> getAllCrews() {
		return crewRepository.findAll();
	}

	public Optional<Crew> assign(Long crewId, Long robotId) {
		Optional<Crew> crew = crewRepository.findById(crewId);
		Optional<Robot> robot = robotRepository.findById(robotId);
		if (!crew.isPresent()) {
			throw new ApplicationException(
					new ErrorDetail("The crew with id [" + crewId +"] was not found.", ErrorCodes.INTERNAL_SERVER_ERROR));
		}
		if (!robot.isPresent()) {
			throw new ApplicationException(
					new ErrorDetail("The robot with id [" + robotId +"] was not found.", ErrorCodes.INTERNAL_SERVER_ERROR));
		}

		robot.get().setCrew(crew.get());
		robotRepository.save(robot.get());

		return crewRepository.findById(crewId);
	}

	public Crew getCrewById(Long crewId) {
		Optional<Crew> crew = crewRepository.findById(crewId);
		if (!crew.isPresent()) {
			throw new ApplicationException(
					new ErrorDetail("The crew with id [" + crewId +"] was not found.", ErrorCodes.INTERNAL_SERVER_ERROR));
		}
		return crew.get();
	}
}
