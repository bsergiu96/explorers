package com.example.crews.core.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.crews.core.domain.Robot;
import com.example.crews.core.repository.AbstractRepository;
import com.example.crews.core.repository.RobotRepository;
import com.example.crews.core.service.common.AbstractService;
import com.example.crews.web.rest.controlers.dto.RobotDTO;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RobotServiceImpl extends AbstractService<Long, Robot, RobotDTO> {
	private final RobotRepository robotRepository;

	public RobotServiceImpl(RobotRepository robotRepository) {
		super(Robot.class);
		this.robotRepository = robotRepository;
	}

	@Override
	protected AbstractRepository<Robot, Long> getRepository() {
		return robotRepository;
	}

	public List<Robot> getAllRobots() {
		return robotRepository.findAll();
	}
}
