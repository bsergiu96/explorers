package com.example.crews.web.rest.common;

public class PathConstants {

	public static final String SEPARATOR = "/";
	public static final String CREWS_PATH_PREFIX = "crews";
	public static final String SWAGGER_UI_PATH = SEPARATOR + CREWS_PATH_PREFIX + "/swagger";
	public static final String WEBJARS_UI_PATH = SEPARATOR + CREWS_PATH_PREFIX + "/webjars";
	public static final String AUTHORIZATION_HEADER_VALUE_PREFIX = "Explorers ";
}
