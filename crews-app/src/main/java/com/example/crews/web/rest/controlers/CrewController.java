package com.example.crews.web.rest.controlers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.crews.core.domain.Crew;
import com.example.crews.core.service.impl.CrewServiceImpl;
import com.example.crews.web.rest.controlers.dto.CrewDTO;

@RestController
@RequestMapping("/crew")
public class CrewController {
	private final CrewServiceImpl crewService;

	public CrewController(CrewServiceImpl crewService) {
		this.crewService = crewService;
	}

	@GetMapping("/allCrews")
	public ResponseEntity<List<Crew>> getAllCrews() {
		return new ResponseEntity<>(crewService.getAllCrews(), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<Crew> getCrewById(@RequestParam(value = "crewId", required = true) Long crewId) {
		return new ResponseEntity<>(crewService.getCrewById(crewId), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<CrewDTO> createCrew(@RequestBody CrewDTO crewDTO) throws URISyntaxException {
		CrewDTO createdCrew = crewService.save(crewDTO);
		return ResponseEntity.created(new URI("/" + createdCrew.getId())).body(createdCrew);
	}

	@PutMapping(value = "/{crewId}/assign/{robotId}")
	public ResponseEntity<CrewDTO> assignRobotToCrew(@PathVariable Long crewId, @PathVariable Long robotId) {
		crewService.assign(crewId, robotId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
