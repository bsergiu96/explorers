package com.example.crews.web.rest.controlers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.crews.core.domain.Robot;
import com.example.crews.core.service.impl.RobotServiceImpl;
import com.example.crews.web.rest.controlers.dto.RobotDTO;

@RestController
@RequestMapping("/robot")
public class RobotController {
	private final RobotServiceImpl robotService;

	public RobotController(RobotServiceImpl robotService) {
		this.robotService = robotService;
	}

	@GetMapping(value = "/allRobots")
	public List<Robot> getAllRobots() {
		return robotService.getAllRobots();
	}

	@PostMapping
	public ResponseEntity<RobotDTO> createRobot(@RequestBody RobotDTO robotDTO) throws URISyntaxException {
		RobotDTO savedRobot = robotService.save(robotDTO);
		return ResponseEntity.created(new URI("/" + savedRobot.getId())).body(savedRobot);
	}
}
