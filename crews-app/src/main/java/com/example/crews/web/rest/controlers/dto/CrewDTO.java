package com.example.crews.web.rest.controlers.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CrewDTO {
	@ApiModelProperty(hidden = true)
	private Long id;
	private String captain;
	@ApiModelProperty(hidden = true)
	private List<RobotDTO> robotDTOList;
}
