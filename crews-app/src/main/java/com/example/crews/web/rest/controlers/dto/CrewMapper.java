package com.example.crews.web.rest.controlers.dto;

import com.example.crews.core.domain.Crew;
import com.example.crews.core.service.common.mapper.Mapper;


@org.mapstruct.Mapper(componentModel = "spring", uses = {})
public interface CrewMapper extends Mapper<Crew, CrewDTO> {

	Crew userDTOToUser(CrewDTO userDTO);

	CrewDTO userToUserDTO(Crew user);

	@Override
	default Crew convertDTOToEntity(CrewDTO crewDTO) {
		return userDTOToUser(crewDTO);
	}

	@Override
	default CrewDTO convertEntityToDTO(Crew crew) {
		return userToUserDTO(crew);
	}

}
