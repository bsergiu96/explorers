package com.example.crews.web.rest.controlers.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RobotDTO {
	@ApiModelProperty(hidden = true)
	private Long id;
	private String name;
}
