package com.example.crews.web.rest.controlers.dto;

import com.example.crews.core.domain.Robot;
import com.example.crews.core.service.common.mapper.Mapper;

@org.mapstruct.Mapper(componentModel = "spring", uses = {})
public interface RobotMapper extends Mapper<Robot, RobotDTO> {

	Robot robotDTOToRobot(RobotDTO robotDTO);

	RobotDTO robotToRobotDTO(Robot robot);

	@Override
	default Robot convertDTOToEntity(RobotDTO robotDTO) {
		return robotDTOToRobot(robotDTO);
	}

	@Override
	default RobotDTO convertEntityToDTO(Robot robot) {
		return robotToRobotDTO(robot);
	}
}
