package com.example.crews.web.rest.interceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.crews.config.NoAuthorizationRequired;
import com.example.crews.web.rest.common.PathConstants;
import com.example.crews.core.exception.ApplicationException;
import com.example.crews.core.exception.ErrorCodes;
import com.example.crews.core.exception.ErrorDetail;
import com.example.crews.core.security.TokenProvider;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
	@Inject
	private TokenProvider tokenProvider;
	@Value("${application.security.authentication.authorizationActive}")
	private boolean authorizationActive;

	public AuthorizationInterceptor() {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (authorizationActive && request != null && response != null && handler != null && request.getRequestURI() != null) {
			if (request.getRequestURI().contains(PathConstants.SWAGGER_UI_PATH) || request.getRequestURI().contains(PathConstants.WEBJARS_UI_PATH)) {
				return true;
			} else if (request.getRequestURI().contains(PathConstants.CREWS_PATH_PREFIX) && handler instanceof HandlerMethod) {
					HandlerMethod method = (HandlerMethod) handler;
					log.info("New request received. URI: [{}]", request.getMethod() + " " + request.getRequestURI());
					if (!method.getMethod().isAnnotationPresent(NoAuthorizationRequired.class)) {
						String jwtToken = tokenProvider.resolveToken(request);
						log.debug("Authorization token: [{}]", jwtToken);
						if (!StringUtils.hasText(jwtToken) || !this.tokenProvider.validateToken(jwtToken)) {
							log.error("Current authentication token [{}] is not valid!", jwtToken);
							throw new ApplicationException(
									new ErrorDetail("Not authorized!", ErrorCodes.UNAUTHORIZED));
						}
					}
			}
		}
		return true;
	}
}
