package com.example.planets;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.example.planets.config.GlobalParameters;
import io.swagger.client.ApiClient;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EnableScheduling
@EnableConfigurationProperties
@EntityScan(basePackages = {"com.example.planets.core.domain"})
public class PlanetApplication extends SpringBootServletInitializer {


	@Bean
	ApiClient client(GlobalParameters globalParameters){
		ApiClient client = new ApiClient();
		client.getHttpClient().setHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});

		client.setBasePath(globalParameters.getBasePath());
		return client;
	}

	@Bean
	GlobalParameters globalParameters() {
		return new GlobalParameters();
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PlanetApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(PlanetApplication.class, args);
	}
}
