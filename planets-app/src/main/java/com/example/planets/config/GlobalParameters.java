package com.example.planets.config;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GlobalParameters implements Serializable {
	@Builder.Default
	String basePath = "http://localhost:8090/crews";
}
