package com.example.planets.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.example.planets.core.domain.enums.PlanetStateEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "planets")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Planet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "image")
	@Lob
	private byte[] image;
	@Enumerated(EnumType.STRING)
	@Column(name = "state")
	private PlanetStateEnum state;
	@Column(name = "crewId")
	private Long crewId;
}
