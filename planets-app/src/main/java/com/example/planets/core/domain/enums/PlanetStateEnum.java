package com.example.planets.core.domain.enums;

public enum PlanetStateEnum {
	OK,
	NOT_OK,
	TODO,
	EN_ROUTE;
}
