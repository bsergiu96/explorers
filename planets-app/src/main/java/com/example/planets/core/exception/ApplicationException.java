package com.example.planets.core.exception;

public class ApplicationException extends RuntimeException{
	private ErrorDetail errorDetail;

	public ApplicationException() {
		super();
	}

	public ApplicationException(ErrorDetail errorDetail) {
		super(errorDetail.getMessage());
		this.errorDetail = errorDetail;
	}

	public ErrorDetail getErrorDetail() {
		return errorDetail;
	}
}
