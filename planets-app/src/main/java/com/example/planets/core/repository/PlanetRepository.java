package com.example.planets.core.repository;

import com.example.planets.core.domain.Planet;

public interface PlanetRepository extends AbstractRepository<Planet, Long> {
}
