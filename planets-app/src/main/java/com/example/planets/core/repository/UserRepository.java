package com.example.planets.core.repository;

import com.example.planets.core.domain.User;

public interface UserRepository extends AbstractRepository<User, Long> {

	User findUserByEmail(String email);
}
