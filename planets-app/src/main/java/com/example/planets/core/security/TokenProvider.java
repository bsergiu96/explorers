package com.example.planets.core.security;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.example.planets.core.domain.User;
import com.example.planets.web.rest.common.PathConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class TokenProvider {

	@Value("${application.security.authentication.jwt.secret}")
	private String secretKey;
	@Value("${application.security.authentication.jwt.token-validity-in-miliseconds}")
	private long tokenValidityInMilliseconds;


	public String createToken(User user) {

		long now = (new Date()).getTime();
		Date validity = new Date(now + this.tokenValidityInMilliseconds);

		return Jwts.builder()
				.setSubject(user.getEmail())
				.signWith(SignatureAlgorithm.HS512, secretKey)
				.setExpiration(validity)
				.claim("idUsername",user.getId())
				.claim("email", user.getEmail())
				.compact();
	}

	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
			return true;
		} catch (Exception e) {
			log.info("Invalid JWT token [{}].", authToken, e);
		}
		return false;
	}

	public String resolveToken(HttpServletRequest request) {
		String bearerToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(PathConstants.AUTHORIZATION_HEADER_VALUE_PREFIX)) {
			return bearerToken.substring(PathConstants.AUTHORIZATION_HEADER_VALUE_PREFIX.length());
		}
		return null;
	}
}
