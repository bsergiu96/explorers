package com.example.planets.core.service.common;

import java.io.Serializable;

import javax.inject.Inject;

import org.springframework.transaction.annotation.Transactional;

import com.example.planets.core.exception.ApplicationException;
import com.example.planets.core.exception.ErrorCodes;
import com.example.planets.core.exception.ErrorDetail;
import com.example.planets.core.repository.AbstractRepository;
import com.example.planets.core.service.common.mapper.Mapper;
import com.example.planets.core.service.common.mapper.MapperService;
import lombok.extern.slf4j.Slf4j;

@Transactional
@Slf4j
public abstract class AbstractService<ID extends Serializable, ENTITY, DTO> {
	private final Class entityClass;
	@Inject
	protected MapperService mapperService;

	protected abstract AbstractRepository<ENTITY, ID> getRepository();

	public AbstractService(Class entityClass) {
		this.entityClass = entityClass;
	}

	public DTO save(DTO dto) {
		try {
			if (dto == null) {
				log.error("This is not a valid entity to be saved. The request DTO was [null]");
				throw new ApplicationException(
						new ErrorDetail("Request not valid", ErrorCodes.BAD_REQUEST)
				);
			}
			ENTITY entity = convertDTOToEntity(dto);
			if (entity == null) {
				log.error("This is not a valid entity to be saved. The request DTO was [{}]", dto);
				throw new ApplicationException(
						new ErrorDetail("Request not valid", ErrorCodes.BAD_REQUEST)
				);
			}
			ENTITY newEntity = getRepository().save(entity);
			return convertEntityToDTO(newEntity);
		} catch (final ApplicationException ex) {
			throw ex;
		} catch (Exception e) {
			log.error("Error occured while saving entity [{}] with using object [{}]", entityClass, dto, 2, e);
			throw new ApplicationException(
					new ErrorDetail("Error occured while saving entity", ErrorCodes.INTERNAL_SERVER_ERROR)
			);
		}
	}

	public ENTITY convertDTOToEntity(final DTO dto) {
		return getMapperService().convertDTOToEntity(dto);
	}

	public DTO convertEntityToDTO(final ENTITY entity) {
		return getMapperService().convertEntityToDTO(entity);
	}

	private Mapper<ENTITY, DTO> getMapperService() {
		return mapperService.getMapperService(entityClass);
	}
}
