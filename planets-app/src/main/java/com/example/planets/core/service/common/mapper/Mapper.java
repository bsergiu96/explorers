package com.example.planets.core.service.common.mapper;

public interface Mapper<ENTITY, DTO> {
	ENTITY convertDTOToEntity(DTO dto);

	DTO convertEntityToDTO(ENTITY entity);
}
