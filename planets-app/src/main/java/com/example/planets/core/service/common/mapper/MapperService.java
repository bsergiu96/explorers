package com.example.planets.core.service.common.mapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.example.planets.core.exception.ApplicationException;
import com.example.planets.core.exception.ErrorCodes;
import com.example.planets.core.exception.ErrorDetail;
import com.google.common.reflect.ClassPath;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MapperService implements ApplicationListener<ContextRefreshedEvent> {
	private Map<String, Mapper> availableMappers = new HashMap<>();

	public Mapper getMapperService(Class classArg) {return availableMappers.get(classArg.getName());}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		final Collection<Mapper> mappers = event.getApplicationContext().getBeansOfType(Mapper.class).values();
		final List<Class> domainClasses = getDomainClasses() ;

		if (domainClasses != null) {
			for (final Class domainClass : domainClasses) {
				mappers.stream().filter(mapper -> match(mapper, domainClass)).forEach(mapper -> {
					availableMappers.put(domainClass.getName(), mapper);
					log.info("Adding mapper for class [{}]", domainClass);
				});
			}
		} else {
			log.warn("No domain classes were found. There is something wrong.");
		}
	}

	private List<Class> getDomainClasses(){
		final ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			final String domainObjectPackage = "com.example.planets.core.domain";
			return ClassPath.from(loader).getAllClasses().stream().filter(classInfo -> {
				return classInfo.getName().contains(domainObjectPackage);
			}).map(ClassPath.ClassInfo::getName).map(className -> className.replaceFirst("BOOT-INF.classes.","")).
					map(className -> {
						try {
							return loader.loadClass(className);
						} catch (ClassNotFoundException e) {
//							LOG.error("Error occurred while search for class {}", e);
							throw new ApplicationException(
									new ErrorDetail("Error occurred while search for class " + e.getMessage(), ErrorCodes.INTERNAL_SERVER_ERROR));
						}
					}).collect(Collectors.toList());
		} catch (Exception e) {
//			LOG.error("Error occurred while getting domain class {}", e);
			throw new ApplicationException(
					new ErrorDetail("Error occurred while getting domain class " + e.getMessage(), ErrorCodes.INTERNAL_SERVER_ERROR));
		}
	}

	private boolean match(Mapper mapper, Class domainClass) {
		return mapper.getClass().getSimpleName().replace("Mapper", "").replace("Impl", "").equals(domainClass
				.getSimpleName());
	}
}
