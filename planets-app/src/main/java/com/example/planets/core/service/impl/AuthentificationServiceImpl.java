package com.example.planets.core.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.planets.core.domain.User;
import com.example.planets.core.exception.ApplicationException;
import com.example.planets.core.exception.ErrorCodes;
import com.example.planets.core.exception.ErrorDetail;
import com.example.planets.core.repository.UserRepository;
import com.example.planets.core.security.TokenProvider;
import com.example.planets.web.rest.authentification.dto.LoginDTO;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class AuthentificationServiceImpl {
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final TokenProvider tokenProvider;

	public AuthentificationServiceImpl(UserRepository userRepository,
			PasswordEncoder passwordEncoder, TokenProvider tokenProvider) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.tokenProvider = tokenProvider;
	}

	public String authenticate(LoginDTO loginDTO) {
		final User userFromDatabase = userRepository.findUserByEmail(loginDTO.getEmail());
		if (userFromDatabase != null && passwordEncoder.matches(loginDTO.getPassword(), userFromDatabase.getPassword())) {
				return tokenProvider.createToken(userFromDatabase);
		}
		log.error("Could not authenticate user with the following credentials: username=[{}] and password=[*******].", loginDTO.getEmail());
		throw new ApplicationException(
				new ErrorDetail("Invalid user or password.", ErrorCodes.AUTHENTICATION_ERROR));
	}
}
