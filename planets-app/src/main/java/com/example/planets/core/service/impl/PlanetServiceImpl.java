package com.example.planets.core.service.impl;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.example.planets.core.domain.Planet;
import com.example.planets.core.domain.enums.PlanetStateEnum;
import com.example.planets.core.exception.ApplicationException;
import com.example.planets.core.exception.ErrorCodes;
import com.example.planets.core.exception.ErrorDetail;
import com.example.planets.core.repository.AbstractRepository;
import com.example.planets.core.repository.PlanetRepository;
import com.example.planets.core.service.common.AbstractService;
import com.example.planets.web.rest.controllers.dto.PlanetDTO;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.CrewControllerApi;
import io.swagger.client.model.Crew;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PlanetServiceImpl extends AbstractService<Long, Planet, PlanetDTO> {
	private final PlanetRepository planetRepository;
	private final ApiClient apiClient;

	public PlanetServiceImpl(PlanetRepository planetRepository, ApiClient apiClient) {
		super(Planet.class);
		this.planetRepository = planetRepository;
		this.apiClient = apiClient;
	}

	@Override
	protected AbstractRepository<Planet, Long> getRepository() {
		return planetRepository;
	}

	public List<Planet> getAllPlanets() {
		return planetRepository.findAll();
	}

	public PlanetDTO uploadImage(Long planetId, byte[] file) {
		PlanetDTO planetDTO = getPlanetById(planetId);

		planetDTO.setImage(file);
		save(planetDTO);

		return planetDTO;
	}

	public PlanetDTO assignCrewToPlanet(Long crewId, Long planetId, HttpServletRequest request) {
		Crew crew = getCrewById(crewId, request);
		PlanetDTO planetDTO = getPlanetById(planetId);

		planetDTO.setCrewId(crew.getId());
		save(planetDTO);

		return planetDTO;
	}

	public PlanetDTO updatePlanetState(String planetState, Long planetId) {
		PlanetDTO planetDTO = getPlanetById(planetId);

		planetDTO.setState(PlanetStateEnum.valueOf(planetState));
		save(planetDTO);

		return planetDTO;
	}

	public PlanetDTO assignCrewToPlanetAndUpdateState(Long crewId, Long planetId, String planetState,
			HttpServletRequest request) {
		Crew crew = getCrewById(crewId, request);
		PlanetDTO planetDTO = getPlanetById(planetId);

		planetDTO.setCrewId(crew.getId());
		planetDTO.setState(PlanetStateEnum.valueOf(planetState));
		save(planetDTO);

		return planetDTO;
	}

	private PlanetDTO getPlanetById(Long planetId) {
		Optional<Planet> planet = planetRepository.findById(planetId);
		if (!planet.isPresent()) {
			throw new ApplicationException(
					new ErrorDetail("The planet with id [" + planetId + "] was not found.", ErrorCodes.INTERNAL_SERVER_ERROR));
		}
		return convertEntityToDTO(planet.get());
	}

	private Crew getCrewById(Long crewId, HttpServletRequest request) {
		apiClient.addDefaultHeader(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
		CrewControllerApi crewControllerApi = new CrewControllerApi(apiClient);
		try {
			Optional<Crew> crew = Optional.ofNullable(crewControllerApi.getCrewByIdUsingGET(crewId));
			if (!crew.isPresent()) {
				throw new ApplicationException(
						new ErrorDetail("The crew with id [" + crewId + "] was not found.", ErrorCodes.INTERNAL_SERVER_ERROR));
			} else {
				return crew.get();
			}
		} catch (ApiException e) {
			throw new ApplicationException(
					new ErrorDetail("The crew with id [" + crewId + "] was not found." + e.getMessage(), ErrorCodes.INTERNAL_SERVER_ERROR));
		}
	}
}
