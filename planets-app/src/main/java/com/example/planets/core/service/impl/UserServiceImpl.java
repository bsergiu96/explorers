package com.example.planets.core.service.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.planets.core.domain.User;
import com.example.planets.core.exception.ApplicationException;
import com.example.planets.core.exception.ErrorCodes;
import com.example.planets.core.exception.ErrorDetail;
import com.example.planets.core.repository.AbstractRepository;
import com.example.planets.core.repository.UserRepository;
import com.example.planets.core.service.common.AbstractService;
import com.example.planets.web.rest.authentification.dto.LoginDTO;
import com.example.planets.web.rest.controllers.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl extends AbstractService<Long, User, UserDTO> {
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;

	public UserServiceImpl(UserRepository userRepository,
			PasswordEncoder passwordEncoder) {
		super(User.class);
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	protected AbstractRepository<User, Long> getRepository() {
		return userRepository;
	}

	public UserDTO createNewUser(LoginDTO loginDTO) {
		validateEmail(loginDTO.getEmail());
		UserDTO userDTO = new UserDTO(loginDTO.getEmail(), loginDTO.getPassword());
		if (userRepository.findUserByEmail(userDTO.getEmail()) != null) {
			log.error("A user with this email is already created");
			throw new ApplicationException(
					new ErrorDetail("A user with the email: [" + userDTO.getEmail() + "] is already created.", ErrorCodes.EMAIL_EXISTS_ERROR));
		} else {
			userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
			save(userDTO);
		}
		return userDTO;
	}

	private void validateEmail(String email) {
		Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		if (!matcher.find()) {
			log.error("This email is not valid!");
			throw new ApplicationException(
					new ErrorDetail("This email is not valid!", ErrorCodes.INVALID_FORMAT_EMAIL_ERROR));
		}
	}
}
