package com.example.planets.web.rest.authentification;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.planets.core.service.impl.AuthentificationServiceImpl;
import com.example.planets.core.service.impl.UserServiceImpl;
import com.example.planets.web.rest.common.PathConstants;
import com.example.planets.config.NoAuthorizationRequired;
import com.example.planets.web.rest.authentification.dto.LoginDTO;
import com.example.planets.web.rest.authentification.dto.TokenDTO;

/**
 * Controller to authenticate users and register users
 */
@RestController
public class AuthenticationResource {

	private final AuthentificationServiceImpl authentificationService;
	private final UserServiceImpl userService;

	public AuthenticationResource(
			AuthentificationServiceImpl authentificationService,
			UserServiceImpl userService) {
		this.authentificationService = authentificationService;
		this.userService = userService;
	}

	@NoAuthorizationRequired
	@PostMapping(value = "/authenticate")
	public ResponseEntity<TokenDTO> authorize(@Valid @RequestBody LoginDTO loginDTO) {
		String jwtToken = authentificationService.authenticate(loginDTO);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(HttpHeaders.AUTHORIZATION, PathConstants.AUTHORIZATION_HEADER_VALUE_PREFIX + jwtToken);
		return new ResponseEntity<>(new TokenDTO(jwtToken), httpHeaders, HttpStatus.OK);
	}


	@NoAuthorizationRequired
	@PostMapping(value = "/register")
	public ResponseEntity register(@RequestBody LoginDTO loginDTO) {
		userService.createNewUser(loginDTO);
		return new ResponseEntity(HttpStatus.OK);
	}
}
