package com.example.planets.web.rest.authentification.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The security token that is stored in cache.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenDTO implements Serializable {
	private String token;
}
