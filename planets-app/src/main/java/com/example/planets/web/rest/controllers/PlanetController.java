package com.example.planets.web.rest.controllers;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.planets.core.domain.Planet;
import com.example.planets.core.domain.enums.PlanetStateEnum;
import com.example.planets.core.exception.ApplicationException;
import com.example.planets.core.exception.ErrorCodes;
import com.example.planets.core.exception.ErrorDetail;
import com.example.planets.core.service.impl.PlanetServiceImpl;
import com.example.planets.web.rest.controllers.dto.PlanetDTO;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/planet")
@Slf4j
public class PlanetController {
	private final PlanetServiceImpl planetService;

	public PlanetController(PlanetServiceImpl planetService) {
		this.planetService = planetService;
	}

	@PostMapping
	public ResponseEntity<PlanetDTO> createPlanet(
			@RequestParam String name) throws URISyntaxException {
		if (name == null || name == "") {
			log.error("This is not a valid entity to be saved. The request name was [null] or empty");
			throw new ApplicationException(
					new ErrorDetail("This is not a valid entity to be saved. The request name was [null] or empty.", ErrorCodes.BAD_REQUEST)
			);
		}
		PlanetDTO planetDTO = new PlanetDTO(name, PlanetStateEnum.TODO);
		PlanetDTO savedPlanet = planetService.save(planetDTO);
		return ResponseEntity.created(new URI("/" + savedPlanet.getId())).body(savedPlanet);
	}

	@PostMapping(value = "/uploadImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PlanetDTO> uploadImage(
			@RequestParam("planetId") Long planetId,
			@RequestParam("file") MultipartFile file) throws IOException, URISyntaxException {
		PlanetDTO savedPlanetDTO = planetService.uploadImage(planetId, file.getBytes());
		return ResponseEntity.created(new URI("/" + savedPlanetDTO.getId())).body(savedPlanetDTO);
	}

	@PutMapping(value = "/{crewId}/assign/{planetId}")
	public ResponseEntity<HttpStatus> assignCrewToPlanet(@PathVariable Long crewId, @PathVariable Long planetId,
			HttpServletRequest request) {
		planetService.assignCrewToPlanet(crewId, planetId, request);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/{planetState}/updateState/{planetId}")
	public ResponseEntity<HttpStatus> updatePlanetState(@PathVariable String planetState, @PathVariable Long planetId) {
		planetService.updatePlanetState(planetState, planetId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/{crewId}/assign/{planetId}/updateState/{planetState}")
	public ResponseEntity<HttpStatus> assignCrewToPlanetAndUpdateState(@PathVariable Long crewId,
			@PathVariable Long planetId,
			@PathVariable String planetState, HttpServletRequest request) {
		planetService.assignCrewToPlanetAndUpdateState(crewId, planetId, planetState, request);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/allPlanets")
	public ResponseEntity<List<Planet>> getAllPlanets() throws URISyntaxException {
		return ResponseEntity.created(new URI("")).body(planetService.getAllPlanets());
	}
}
