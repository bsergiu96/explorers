package com.example.planets.web.rest.controllers.dto;

import lombok.Data;

@Data
public class PlanetCreateRequestDTO {
	private String name;
	private byte[] image;
}
