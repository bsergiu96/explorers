package com.example.planets.web.rest.controllers.dto;

import org.springframework.web.multipart.MultipartFile;

import com.example.planets.core.domain.enums.PlanetStateEnum;
import io.swagger.client.model.Crew;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlanetDTO {
	private Long id;
	private String name;
	private byte[] image;
	private PlanetStateEnum state;
	private Long crewId;

	public PlanetDTO(String name, PlanetStateEnum state) {
		this.name = name;
		this.state = state;
	}
}
