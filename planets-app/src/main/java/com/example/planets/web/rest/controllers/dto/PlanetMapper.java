package com.example.planets.web.rest.controllers.dto;

import com.example.planets.core.domain.Planet;
import com.example.planets.core.service.common.mapper.Mapper;
import io.swagger.client.model.Crew;

@org.mapstruct.Mapper(componentModel = "spring", uses = {})
public interface PlanetMapper extends Mapper<Planet, PlanetDTO> {

//	@Mapping(source = "crew", target = "crewId")
	Planet planetDTOToPlanet(PlanetDTO planetDTO);

//	@Mapping(source = "crewId", target = "crew")
	PlanetDTO planetToPlanetDTO(Planet planet);

	default Crew crewFromId(Long id) {
		if (id == null) {
			return null;
		}
		Crew crew = new Crew();
		crew.setId(id);
		return crew;
	}

	@Override
	default Planet convertDTOToEntity(PlanetDTO planetDTO){
		return planetDTOToPlanet(planetDTO);
	}

	@Override
	default PlanetDTO convertEntityToDTO(Planet planet) {
		return planetToPlanetDTO(planet);
	};
}
