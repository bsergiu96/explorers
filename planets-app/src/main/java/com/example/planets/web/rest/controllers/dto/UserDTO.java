package com.example.planets.web.rest.controllers.dto;

import lombok.Data;

@Data
public class UserDTO {
	private Long id;
	private String email;
	private String password;

	public UserDTO() {
	}

	public UserDTO(String email, String password) {
		this.email = email;
		this.password = password;
	}


}
