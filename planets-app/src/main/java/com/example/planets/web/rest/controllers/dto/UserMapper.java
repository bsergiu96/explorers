package com.example.planets.web.rest.controllers.dto;

import com.example.planets.core.domain.User;
import com.example.planets.core.service.common.mapper.Mapper;

@org.mapstruct.Mapper(componentModel = "spring", uses = {})
public interface UserMapper extends Mapper<User, UserDTO> {

	User userDTOToUser(UserDTO userDTO);

	UserDTO userToUserDTO(User user);

	@Override
	default User convertDTOToEntity(UserDTO userDTO){
		return userDTOToUser(userDTO);
	}

	@Override
	default UserDTO convertEntityToDTO(User user) {
		return userToUserDTO(user);
	};
}
