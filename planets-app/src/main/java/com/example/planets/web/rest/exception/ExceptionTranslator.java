package com.example.planets.web.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.planets.core.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller advice to translate the server side exceptions to client - fr
 */
@ControllerAdvice
@Slf4j
public class ExceptionTranslator {

	public ExceptionTranslator() {
	}

	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<?> processApplicationException(ApplicationException ex) {
		log.error("Exception appear with following details : ", ex);
		if (ex.getErrorDetail() != null) {
			return new ResponseEntity<>(ex.getErrorDetail(), HttpStatus.valueOf(ex.getErrorDetail().getHttpStatusCode()));
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
